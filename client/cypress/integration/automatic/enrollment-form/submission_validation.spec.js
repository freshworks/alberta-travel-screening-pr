import { 
  passContactStep, 
  fillPrimaryContact, 
  fillArrivalInformation, 
  fillIsolationQuestionnaire, 
  fillAdditionalTravellers 
} from '../../utils/utils';

describe("Fails submission validation", () => {
  beforeEach(() => {
    // reset and pass eligibility validation
    cy.visit("http://localhost:3000/enroll");
    passContactStep();
  })

  it("Fails submission due to empty field - Exempt status missing field", () => {

    // Marks form primary as exempt
    cy.get('[value="Exempt"]').click();
    
    cy.get('[id="mui-component-select-exemptOccupation"]').click();
    cy.get('[data-value="Crew member (air)"]').click();
    
    cy.contains("Continue").click();

    cy.contains("Must specify occupation details")
  })

  it("Fails submission due to empty field - Additional traveller missing field", () => {
    //Fill primary contact portion
    fillPrimaryContact();

    // Marks form primary as non-exempt
    cy.get('[value="Non-Exempt"]').click();

    //Fill arrival information portion
    fillArrivalInformation();
    
    cy.get('[name="hasAdditionalTravellers"]').check("Yes");
    cy.get('[id="mui-component-select-numberOfAdditionalTravellers"]').click();
    cy.get('[data-value="1"]').click();

    fillAdditionalTravellers({numberOfFieldSets: 1, omitOne: true});

    
    fillIsolationQuestionnaire({omitOne: false});

    cy.contains("Continue").click();

    cy.contains("Must specify gender");
  })

  it("Fails submission due to empty field - basic fill, no dynamic fields", () => {
    //Fill primary contact portion
    fillPrimaryContact();

    // Marks form primary as non-exempt
    cy.get('[value="Non-Exempt"]').click();

    //Fill arrival information portion
    fillArrivalInformation();

    //Marks additional members and travellers outside household as No
    cy.get('[name="hasAdditionalTravellers"]').check("No");

    //Fill isolation questionnaire portion, pass omit flag to not fill one field
    fillIsolationQuestionnaire({omitOne: true});

    cy.contains("Continue").click();

    cy.contains("Address is required")
  })
})