
import { correctSelection, passEligibilityStep } from '../utils/utils';

describe("Eligibility Form", () => {
  beforeEach(() => {
    // reset
    cy.visit("http://localhost:3000/enroll");
  })


  describe("Fails eligibility check", () => {
    it ("Fails by checking all radio buttons as 'Yes'", () => {
      cy.get('[type="radio"]').check('Yes');

      cy.contains('Continue').click();

      cy.contains('You are not eligible');
    })

    it ("Fails by checking all radio buttons as 'No'", () => {
      cy.get('[type="radio"]').check('No');

      cy.contains('Continue').click();

      cy.contains('You are not eligible');
    })

    it ("Fails eligibility on residency check", () => {
      correctSelection();

      cy.get('[name="residency"]').check('No');

      cy.contains('Continue').click();

      cy.contains('You are not eligible');
    })

    it ("Fails eligibility on symptoms check", () => {
      correctSelection();

      cy.get('[name="symptoms"]').check('Yes');

      cy.contains('Continue').click();

      cy.contains('You are not eligible');
    })

    it ("Fails eligibility on contact with covid check", () => {
      correctSelection();

      cy.get('[name="covidContact"]').check('Yes');

      cy.contains('Continue').click();

      cy.contains('You are not eligible');
    })

    it ("Fails eligibility on quarantine check", () => {
      correctSelection();
      
      cy.get('[name="quarantinePlan"]').check('No');

      cy.contains('Continue').click();

      cy.contains('You are not eligible');
    })

    it ("Fails eligibility on length of stay check", () => {
      correctSelection();
      cy.get('[name="remainingInAlberta"]').check('No');

      cy.contains('Continue').click();

      cy.contains('You are not eligible');
    })

  })

  describe('Passes eligibility check', () => {
    it ("Passes eligibility with the valid selection", () => {
      passEligibilityStep();
    })
  })
  
})