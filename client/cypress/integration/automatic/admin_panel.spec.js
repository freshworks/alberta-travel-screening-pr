import moment from "moment";
import { completeDailyForHousehold, correctSelection, fillAdditionalTravellers, fillArrivalInformation, fillIsolationQuestionnaire, fillPrimaryContact, findHousehold, loginAdminPortal2FA } from "../utils/utils";


/** disables localStorage automatically clearing on each test for this test suite.
 *  Behavior is manually replicated in the beforeEach / afterEach of the test describe.
 *  This is due to Cypress changing localStorage incorrectly and clearing it before each 'it' runs.
*/
const clear = Cypress.LocalStorage.clear

Cypress.LocalStorage.clear = function (keys, ls, rs) {
  return;
}

describe("Service Alberta Portal", () => {
  const datestring = moment().format('x'); // little 'x' denotes unix timestamp with milliseconds
  let redirectURL = '';
  beforeEach(async() => {
    await loginAdminPortal2FA()
  })

  afterEach(()=> {
  })

  it("Logs in successfully with existing session and navigates to the monitoring portal", () => {
    cy.visit('http://localhost:3000/monitoring')
    cy.contains("All Submissions");
    
  })

  it("Enrolls a household through the monitoring portal dialog", () => {
    cy.visit('http://localhost:3000/monitoring')

    cy.contains("Submit Enrollment Form").click();

    correctSelection();
    // Cypress doesn't like dialogs so you need to access the DOM directly
    cy.get('[type="submit"]').contains("Submit").click();

    fillPrimaryContact();
    cy.get('[value="Non-Exempt"]').click();
    cy.get('[name="firstName"]').type(` Monitoring-${datestring}`);

    fillArrivalInformation();

    cy.get('[name="hasAdditionalTravellers"]').check("Yes");
    cy.get('[id="mui-component-select-numberOfAdditionalTravellers"]').click();
    cy.get('[data-value="2"]').click();
    fillAdditionalTravellers({numberOfFieldSets: 2, omitOne: false});

    fillIsolationQuestionnaire({omitOne: false});
    
    // Cypress doesn't like dialogs so you need to access the DOM directly
    cy.get('[type="submit"]').contains("Submit").click();
    cy.contains("Your application will be verified upon entry into Canada. Please make note of your confirmation number(s).")
  })
  
  it("Verifies creation of household", () => {
    cy.visit('http://localhost:3000/monitoring')

    // Change table view to all and search by name
    findHousehold({datestring: datestring, filter: 'All'});

    cy.contains(`Automated Monitoring-${datestring}`)
  })

  it("Verifies applied status of household members", () => {
    cy.visit('http://localhost:3000/monitoring')

    // Change table view to all and search by name
    findHousehold({datestring: datestring, filter: 'All'});

    cy.contains(`Automated Monitoring-${datestring}`)
    cy.contains("View").click();

    // Assert each traveller was submitted 'today'
    cy.get('tbody > tr').should("have.length", 3).each(($el) => {
    cy.wrap($el).contains('applied');
    })
  })

  it("Views and enrolls the created household", () => {
    cy.visit('http://localhost:3000/monitoring')

    // Change table view to all and search by name
   findHousehold({datestring: datestring, filter: 'All'});

    // Go to household details view
    cy.contains(`Automated Monitoring-${datestring}`);
    cy.contains("View").click();

    // Enroll the household in the pilot program
    cy.contains(`Automated Monitoring-${datestring} Test`);
    cy.get('[data-id="householdAction"]').click();
    cy.get('[role="menu"] > [data-id="Enroll"]:visible').contains("Enroll").click();
    cy.get('[type="button"]').contains("Yes").click();
    cy.contains("Household enrolled!");
  })

  it("Verifies enrolled status of household members", () => {
    cy.visit('http://localhost:3000/monitoring')

    // Change table view to all and search by name
    findHousehold({datestring: datestring, filter: 'All'});

    cy.contains(`Automated Monitoring-${datestring}`)
    cy.contains("View").click();

    // Assert each traveller was submitted 'today'
    cy.get('tbody > tr').should("have.length", 3).each(($el) => {
    cy.wrap($el).contains('enrolled');
    })
  })

  it("Generates a daily reminder link for the created household", () => {
    cy.visit('http://localhost:3000/monitoring')

    // Change table view to all and search by name
    findHousehold({datestring: datestring, filter: 'All'});

    // Go to household details view
    cy.contains(`Automated Monitoring-${datestring}`);
    cy.contains("View").click();

    // Generate daily reminder link
    cy.contains(`Automated Monitoring-${datestring} Test`);
    cy.contains("Generate daily reminder link").click();
    cy.contains("Generated daily questionnaire link!");
    cy.contains("enrolled");
  })

  it("Edits the arrival date of the created household to 3 days ago", () => {
    cy.visit('http://localhost:3000/monitoring')

    // Change table view to all and search by name
    findHousehold({datestring: datestring, filter: 'All'});

    // Go to household details view
    cy.contains(`Automated Monitoring-${datestring}`);
    cy.contains("View").click();

    // Edits the arrival date
    cy.contains(`Automated Monitoring-${datestring} Test`);
    cy.contains("Edit").click();
    cy.get('[name="arrivalDate"]').clear().type(moment().subtract(2,'days').format('YYYY/MM/DD'));
    cy.get('[type="submit"]').contains("Submit").click();
    cy.contains(moment().subtract(2, 'days').format('DD MMM YYYY'))
  })

  it("Captures a valid redirect URL for the household daily survey", () => {
    cy.visit('http://localhost:3000/monitoring')

    // Change table view to all and search by name
    findHousehold({datestring: datestring, filter: 'All'});

    // Go to household details view
    cy.contains(`Automated Monitoring-${datestring}`);
    cy.contains("View").click();
    
    // Intercept window open call
    cy.contains(`Automated Monitoring-${datestring} Test`);
    cy.window().then((win) => {
      cy.stub(win, 'open')
        .as('windowOpen')
        .callsFake((url, target, features, replace) => (
          // Store URL for further tests
          redirectURL = url
        ))
    })

    // Trigger window event
    cy.contains("Submit for household").click();
  })


  it("submits the daily survey on behalf of the household", () => {
    cy.visit(redirectURL)

    completeDailyForHousehold({numberOfSurveys: 3, fillBirthdate: false});

    cy.contains("Thank you for submitting your symptoms.");
  })

  it("Verifies household enrollment", () => {
    cy.visit('http://localhost:3000/monitoring')

    // Change table view to Green and search by name
    findHousehold({datestring: datestring, filter: 'Green'});

    // Verify household enrollment status in table
    cy.contains(`Automated Monitoring-${datestring}`);
    cy.contains("ENROLLED");
  })

  it("Verifies daily survey submission for each traveller", () => {
    cy.visit('http://localhost:3000/monitoring')

    // Change table view to Green and search by name
    findHousehold({datestring: datestring, filter: 'Green'});

    // // Go to household details view
    cy.contains(`Automated Monitoring-${datestring}`);
    cy.contains("View").click();

    // Assert each traveller is green
    cy.contains(`Automated Monitoring-${datestring} Test`);
    cy.get('td > [class="MuiChip-root MuiChip-outlined"]').should("have.length", 3).each(($el) => {
      cy.wrap($el).contains("GREEN");
    });

    // Assert each traveller was submitted 'today'
    cy.get('tbody > tr').should("have.length", 3).each(($el) => {
      cy.wrap($el).contains(moment().format("DD MMM YYYY"));
    })
  })

  it("Withdraws household", () => {
    cy.visit('http://localhost:3000/monitoring')

    // Change table view to Green and search by name
    findHousehold({datestring: datestring, filter: 'Green'});

    // // Go to household details view
    cy.contains(`Automated Monitoring-${datestring}`);
    cy.contains("View").click();

    // Assert each traveller is green
    cy.contains(`Automated Monitoring-${datestring} Test`);
    cy.get('[data-id="householdAction"]').click();
    cy.get('[role="menu"] > [data-id="Withdraw"]:visible').contains("Withdraw").click();
    cy.get('[type="submit"]').contains("Confirm").click();
    cy.contains("Household withdrawn!");
  })

  it("Verifies household withdrawl", () => {
    cy.visit('http://localhost:3000/monitoring')

    // Change table view to Green and search by name
    findHousehold({datestring: datestring, filter: 'Green'});

    // Verify household withdrawl status in table
    cy.contains(`Automated Monitoring-${datestring}`);
    cy.contains("WITHDRAWN");
  })

  it("Verifies withdrawn status of household members", () => {
    cy.visit('http://localhost:3000/monitoring')

    // Change table view to all and search by name
    findHousehold({datestring: datestring, filter: 'All'});

    cy.contains(`Automated Monitoring-${datestring}`)
    cy.contains("View").click();

    // Assert each traveller was submitted 'today'
    cy.get('tbody > tr').should("have.length", 3).each(($el) => {
    cy.wrap($el).contains('withdrawn');
    })
  })

  it("Assigns household to test admin and adds a comment to the household", () => {
    cy.visit('http://localhost:3000/monitoring')

    // Change table view to Green and search by name
    findHousehold({datestring: datestring, filter: 'Green'});

    // // Go to household details view
    cy.contains(`Automated Monitoring-${datestring}`);
    cy.contains("View").click();

    // Assign household and submit comment
    cy.contains(`Automated Monitoring-${datestring} Test`);
    cy.get('[type="button"]').contains("Assign To Me").click();
    cy.contains("has been assigned to you.");
    cy.get('[id="mui-component-select-status"]').click();
    cy.get('[data-value="No answer, 3 call attempts made"]').click();
    cy.get('[name="note"]').type("Withdrawn by automated test suite.");
    cy.get('[data-id="note-submit"]').click();
    cy.get('[type="button"]').contains("Yes, submit now.").click();
    cy.contains("Withdrawn by automated test suite.");
  })

    it("Enrolls a single household member", () => {
      cy.visit('http://localhost:3000/monitoring')
      
      // Change table view to all and search by name
      findHousehold({datestring: datestring, filter: 'All'});

      // cy.contains(`Automated Monitoring-${datestring}`)
      cy.contains("View").click();

      // Enrolls the first traveller in the table
      cy.get('tbody > tr').should("have.length", 3).each(($el, index) => {
        if (index === 0) {
          cy.wrap($el).contains('...').click();
          cy.get('[role="menu"] > [data-id="Enroll"]:visible').contains("Enroll").click();
          cy.get('[type="button"]').contains("Yes").click();
          cy.contains("Traveller enrolled!")

        }
      })

      // Asserts only the first traveller is enrolled
      cy.get('tbody > tr').should("have.length", 3).each(($el, index) => {
        if (index === 0) {
          cy.wrap($el).contains('enrolled');
        } else {
          cy.wrap($el).contains('withdrawn');

        }
      })

      //Verifies household status has changed with the individual enrollment status
      cy.get("p").contains("enrolled")

    })

    it("Withdraws a single household member", () => {
      cy.visit('http://localhost:3000/monitoring')
      
      // Change table view to all and search by name
      findHousehold({datestring: datestring, filter: 'All'});

      // cy.contains(`Automated Monitoring-${datestring}`)
      cy.contains("View").click();

      // Withdraws the first traveller in the table
      cy.get('tbody > tr').should("have.length", 3).each(($el, index) => {
        if (index === 0) {
          cy.wrap($el).contains('...').click();
          cy.get('[role="menu"] > [data-id="Withdraw"]:visible').contains("Withdraw").click();
          cy.get('[type="submit"]').contains("Confirm").click();
          cy.contains("Traveller withdrawn!")

        }
      })

      // Asserts that the first traveller is explicitly withdrawn, double check the others
      cy.get('tbody > tr').should("have.length", 3).each(($el, index) => {
        if (index === 0) {
          cy.wrap($el).contains('withdrawn');
        } else {
          cy.wrap($el).contains('withdrawn');

        }
      })

      //Verifies household status has changed with the individual enrollment status
      cy.get("p").contains("withdrawn")

    })

});