import * as yup from 'yup';

const validateDateString = (s) => /^\d{4}\/\d{2}\/\d{2}$/.test(s);

export const ReportValidationSchema = yup.object().shape({
    report: yup.string().required("Report is required"),
    fromDate: yup.string().required("From date is required").test('is-date', "Not a valid date", validateDateString),
    toDate: yup.string().required("End date is required").test('is-date', "Not a valid date", validateDateString).when('fromDate', (fromDate, schema) => {
        return yup.date().min(fromDate, "To date cannot be lesser than From date");
    }),
});


export const ReportFilterValidationSchema = yup.object().shape({
    fromDate: yup.string().optional().nullable(),
    toDate: yup.string().optional().nullable(),
    agent: yup.string().optional().nullable(),
    query: yup.string().optional().nullable().min(3, 'Value entered must be 3 characters or more').max(64, 'Value entered must be 64 characters or less'),
});