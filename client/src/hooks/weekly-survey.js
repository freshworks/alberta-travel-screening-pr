  import { useState } from 'react';
import { useHistory } from 'react-router-dom';

import { AxiosPublic } from '../utils';
import { useToast } from '.';

export const useWeeklySurvey = () => {
  const history = useHistory();
  const [isFetching, setFetching] = useState(false);
  const [hasSent, setHasSent] = useState(false);
  const [response, setResponse] = useState();
  const { openToast } = useToast();

  return {
    isFetching,
    hasSent,
    response,
    reset: () => setHasSent(false),
    submit: async (token, surveyType, values) => {
      try {
        setHasSent(false);
        setFetching(true);
        setResponse(await AxiosPublic.post(`/api/v2/rtop/traveller/${token}/surveyQuestionnaire`, {surveyType, answers: values}));
        openToast({ status: 'success', message: "Survey completed successfully!" });
        setHasSent(true);
      } catch (e) {
        openToast({ status: 'error', message: e.message || "Failed to send verification" });
      } finally {
        setFetching(false);
      }
    }
  };
}