import React, { useEffect, useState } from 'react';

import { AxiosPrivate, getOptionalURLParams } from "../utils";
import { useHistory } from 'react-router-dom';

import { useAuth } from '.';

export const useListActiveAgents = () => { 
    const history = useHistory();

    const [isFetching, setFetching] = useState(false);
    const [hasExecuted, setHasExecuted] = useState(false);
    const [initialAgent, setInitialAgent] = useState(null);
    const [options, setOptions] = useState([{
        value: 'all', label: 'All'
    }]);

    useEffect(() => {
        (async () => {
            // Find and decode agent name if specified with `agent` url param
            const { agent=null } = getOptionalURLParams(history);
            let agentName = null;

            try {
                agentName = agent? decodeURIComponent(agent): null;
            } catch {}

            setFetching(true);
            
            // Retrieve all activate agents
            await AxiosPrivate.get(`/api/v2/rtop-admin/agents`)
                .then(agents => {

                    const options = [{value: 'all', label: 'All'}].concat(
                        agents && agents.length? agents.map(a => ({
                            value: a.name,
                            label: a.name
                        })): []
                    );
                
                    // Select specified agent if name matches the specified `agent` url param
                    const initialAgent = agents.find(a => a.name === agentName);

                    setOptions(options);

                    if(initialAgent) {
                        setInitialAgent(initialAgent);
                    }
                })
                .finally(() => {
                    setFetching(false);
                    setHasExecuted(true);
                });
        })();
    }, []);

    return {
        isFetching,
        initialAgent,
        hasExecuted,
        options,
        setInitialAgent,
    }
}

export const useReportsAccessControl = () => {
    const [reportAccess, setReportAccess] = useState({ hasAccess: false, reports: [] });
    const { clearAuthState, state: { userType, isAuthenticated, user } } = useAuth();

    useEffect(() => {
        (async () => {
            if ( user && user.idTokenPayload && user.idTokenPayload.email )
            {
                const { agentEmail, reportName = [] } = await AxiosPrivate.get(`/api/v1/admin/report/access/${user.idTokenPayload?.email}`);
                if (reportName.length > 0) {
                    setReportAccess({ hasAccess: true, reports: reportName });
                }
            }
        })();

    }, []);

    return {
        reportAccess
    }

}