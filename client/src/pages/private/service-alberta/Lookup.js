import React, { useRef } from 'react';

import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import { useHistory } from 'react-router-dom';
import { ServiceAlbertaLookupFilters } from '../../../constants';
import { useServiceAlbertaLookup } from '../../../hooks';
import { withStyles } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';

import { Page, Filter, Table, Button } from '../../../components/generic';
import { getOptionalURLParams } from '../../../utils';
import { FastField, Formik, Form as FormikForm } from 'formik';
import { RenderDateField , RenderTextField, RenderSelectField} from '../../../components/fields';
import { ReportFilterValidationSchema } from '../../../constants/report-validation';
import { useListActiveAgents } from '../../../hooks/admin';
import ClearIcon from "@material-ui/icons/Clear";

export default () => {
  const history = useHistory();
  const {
    onFilter,
    onChangePage,
    onSort,
    onSubmit,
    clearFilter,
    initialFilters,
    zeroStates,
    selectedSearchFilters,
    tableData,
    isFetching,
  } = useServiceAlbertaLookup();

  const {
    initialAgent,
    hasExecuted: hasFetchedAgents,
    options
  } = useListActiveAgents();

  const { filter: filterValue = ServiceAlbertaLookupFilters[1].value } = getOptionalURLParams(history);
  
  const initialValues = Object.assign({}, initialFilters, {agent: (initialAgent && initialAgent.name) || 'all'});  

  // Reference to formik form instance
  const lookupForm = useRef();

  const resetFilter = (field) => {
    if(field === 'date') {
      resetFilter('fromDate');
      resetFilter('toDate');
      return;
    }

    if(lookupForm.current) {
      lookupForm.current.setFieldValue(field, zeroStates[field] || '');
    }

    clearFilter(field);
  };

  const StyledDateSelector = withStyles((theme) => ({
    root: {
      [theme.breakpoints.up('md')]: {
        maxWidth: '220px'
      }
    },
  }))(FastField);
  

  return (
    <Page hideFooter>

      {/** Blue Banner */}
      <Box pt={[3, 4, 6]} pb={[3, 4, 6]} bgcolor="secondary.main" color="common.white">
        <Container maxWidth="md">
          <Typography variant="h1">All Submissions</Typography>
        </Container>
      </Box>

      {/** White Banner */}
      {/* <Grid container xs={12}> */}
      <Box pt={[3, 3]} pb={[3, 3]}>
        {initialFilters && hasFetchedAgents && <Formik
            validationSchema={ReportFilterValidationSchema}
            initialValues={initialValues}
            innerRef={lookupForm}
            onSubmit={values => onSubmit(values)}>
          <FormikForm>
            <Container maxWidth="md">
              <Grid container justify="space-between" alignItems="center" spacing={2}>
                <Grid item style={{position: 'relative'}}>
                  <StyledDateSelector
                      name="fromDate"
                      label="From Date (YYYY/MM/DD)"
                      component={RenderDateField}
                      placeholder="YYYY/MM/DD"
                      disableFuture={false}
                  />
                </Grid>
                <Grid item>
                  <StyledDateSelector
                      name="toDate"
                      label="To Date (YYYY/MM/DD)"
                      component={RenderDateField}
                      placeholder="YYYY/MM/DD"
                      disableFuture={false}
                  />
                </Grid>
                <Grid item style={{minWidth: '190px', maxWidth: '190px'}}>
                  <FastField
                      name="agent"
                      label="Agent"
                      options={options}
                      component={RenderSelectField}
                    />
                </Grid>
                <Grid item>
                  <FastField
                      name="query"
                      placeholder="Search for Name or Confirmation #"
                      label="Name OR Confirmation #"
                      component={RenderTextField}
                    />
                </Grid>
                <Grid item style={{textAlign: 'center', marginTop: '30px'}}>
                  <label>&nbsp;</label>
                  <Button
                      style={{minWidth: '100px', padding: '10px 10px'}}
                      text="Search"
                      type="submit"
                      color="primary"
                      size="small"
                      endIcon={<SearchIcon></SearchIcon>}
                      fullWidth={false}
                  />
                </Grid>
              </Grid>
            </Container>
          </FormikForm>
        </Formik>
      }
      </Box>

      {/** Table */}
      <Box py={3}>

        <Container maxWidth="md" style={{paddingBottom: '12px'}}>
          <Grid container justify="space-between" alignItems="center" spacing={2}>
            <Grid item>
              <Grid container alignItems="center" spacing={2}>
                <Grid item>
                  <Typography variant="subtitle2" color="textSecondary">Filter:</Typography>
                </Grid>
                <Grid item>
                  <Filter
                    onClick={onFilter}
                    options={ServiceAlbertaLookupFilters}
                    value={filterValue}
                  />
                </Grid>
              </Grid>
            </Grid>
            <Grid item>
              <Grid container alignItems="center" spacing={2}>
                <Grid item>
                </Grid>
                <Grid item>
                  <Filter
                    endIcon={<ClearIcon/>}
                    onClick={field => resetFilter(field)}
                    options={selectedSearchFilters}
                    alwaysSelected={true}>
                  </Filter>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Container>
        <Container maxWidth="md">
          <Table
            columns={tableData.columns}
            rows={tableData.rows}
            totalRows={tableData.totalRows}
            currentPage={tableData.currentPage}
            rowsPerPage={20}
            isLoading={isFetching}
            onChangePage={onChangePage}
            onSort={onSort}
          />
        </Container>
      </Box>
    </Page>
  );
};
