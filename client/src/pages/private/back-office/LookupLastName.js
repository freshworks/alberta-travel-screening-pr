import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import { useHistory } from 'react-router-dom';

import { useBackOfficeLookupLastName, useValidateLookupQuery } from '../../../hooks';

import { Page, Table, SearchBar } from '../../../components/generic';
import { getOptionalURLParams } from '../../../utils';

export default () => {
  const history = useHistory();
  const { onSearch, tableData, isFetching } = useBackOfficeLookupLastName();
  const { validateQuery } = useValidateLookupQuery();
  const { query = '' } = getOptionalURLParams(history);

  return (
    <Page hideFooter>
      <Grid container justify="center">
        <Grid item xs={12} sm={12} md={10} lg={8} xl={6}>
          <Box m={4}>
            <Grid container alignItems="center" justify="space-between" spacing={3}>

              {/** Title */}
              <Grid item xs={12}>
                <Typography variant="h3" color="primary">
                  Submission Lookup
                </Typography>
              </Grid>

              {/** Search Bar */}
              <Grid item xs={12} sm={6}>
                <SearchBar
                  placeholder="Search last name..."
                  initialValue={query}
                  onSearch={(value) => validateQuery(value, onSearch)}
                />
              </Grid>

              {/** Table */}
              <Grid item xs={12}>
                <Table
                  columns={tableData.columns}
                  rows={tableData.rows}
                  isLoading={isFetching}
                />
              </Grid>
            </Grid>
          </Box>
        </Grid>
      </Grid>
    </Page>
  );
};
