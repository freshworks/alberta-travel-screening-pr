import React from 'react';
import Grid from '@material-ui/core/Grid';
import { Formik, Form as FormikForm, Field } from 'formik';
import { Box } from '@material-ui/core';
import { Link } from 'react-router-dom';
import { FastField } from 'formik';

import { Card, Button, FocusError } from '../../generic';
import { RenderSmsCodeField } from '../../fields';
import { SmsVerification } from '../../../constants';


export const VerifySMS = ({onSubmit, onReset}) => (
  <Formik
    initialValues={{verificationCode: ''}}
    validationSchema={SmsVerification}
    onSubmit={onSubmit}
  >
    {({values}) => (
    <FormikForm>
        <FocusError />
        <Card title='Verify SMS'>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <Field
                name="verificationCode"
                label="Please enter the verification code recieved via SMS*"
                placeholder="Required"
                component={RenderSmsCodeField}
              />
            </Grid>
            <Grid item xs={12} sm={7} md={6} lg={4} xl={2}>
              <Box ml={[1, 0]} mr={[1, 0]}>
                <Button
                  text="Verify"
                  type="submit"
                />
              </Box>
            </Grid>
            <Grid item xs={12} alignItems="center" container justify="center">
                <Link to="#" onClick={onReset}>I didn't receive a link</Link>
            </Grid>
          </Grid>            
        </Card>
    </FormikForm>
    )}
  </Formik>
);