import { APIStrategy } from "ibmcloud-appid";
import { PassportStrategy } from "@nestjs/passport";
import { Injectable } from "@nestjs/common";
import { LoggerService } from "../../logs/logger";
import { AppIDConfig } from "../envconfig";

@Injectable()
export class OthersPassportStrategy extends PassportStrategy(APIStrategy, "others") {
  /**
   * Sets up Others App ID configuration
   * Implements APIStrategy for passport
   */
  constructor() {
    // AppID configuration
    const envConfig = AppIDConfig('OTHER');

    super({
      oauthServerUrl: (envConfig.OAUTH_SERVER_URL || 'aurl').trim(),
      tenantId: (envConfig.TENANT_ID ||'anid').trim(),
    });
    LoggerService.info("OTHERS APP ID registered");
  }
}
