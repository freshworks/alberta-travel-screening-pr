import fs from 'fs';
import { Logger } from '@nestjs/common';

export enum DAILY_STATUS {
    RED = 'red',
    GREEN = 'green',
    YELLOW = 'yellow'
}

export enum CARD_STATUS_REASON {
    DAILY_SYMPTOM_FLAG = 'Daily Symptom Flag: Traveller has one of COVID symptoms',
    DAILY_TRACKING_FLAG = 'Daily Tracking not Completed: Traveller has not completed daily checkin by midnight',
    NOT_TESTED = 'Not performed 2nd COVID test',
    ABTRACETOGETHER_FLAG = 'Not using ABTracetogether App',
}

class DailyQuestionAnswer {
    yes: string;
    no: string;
}

export const DAILY_CHECK_OTHERS = ['usingABTraceTogether'];

/**
 * Parse questions from daily questionnaire from either a file
 * or from env variable directly.
 * 
 * process.env.DAILY_QUESTIONS_FILE: JSON file with an object following the format below
 * process.env.DAILY_QUESTIONS_FILE: env variable containing an object following the format below
 * 
 * {
 *     <question_identifier>: {
 *         "<answer>": <answer token>
 *         ...
 *     }
 *     ...
 * }
 * 
 */
export const DAILY_QUESTIONS: Record<string, DailyQuestionAnswer> = (() => {
    if(process.env.DAILY_QUESTIONS_FILE) {
        try {
            const questions = fs.readFileSync(process.env.DAILY_QUESTIONS_FILE, 'utf-8');
            return Object.freeze(JSON.parse(questions));
        } catch(e) {
            Logger.error('Failed to parse questions file', e);
            throw e
        }
    } else if(process.env.DAILY_QUESTIONS) {
        return Object.freeze(JSON.parse(process.env.DAILY_QUESTIONS));
    } else {
        Logger.error('No questions file or questions env variable specified');

        throw new Error(`Either DAILY_QUESTIONS_FILE or DAILY_QUESTIONS env variable must be defined`);
    }
})();

export const ENROLMENT_REPORT_EXPORT = {
    'ENROLLMENT_DATE': 'Date of enrolment',
    'ARRIVAL_DATE': 'Arrival Date',
    'FIRST_NAME': 'First Name',
    'LAST_NAME': 'Last Name',
    'DATE_OF_BIRTH': 'Birth Date',
    'PHONE_NUMBER': 'Primary Phone Number',
    'ISOLATION_PHONE_NUMBER': 'Phone Number at Isolation Location',
    'EXEMPT': 'Exempt'
};

export const WITHDRAWAL_REPORT_EXPORT = {
    'ENROLLMENT_DATE': 'Date of enrolment',
    'WITHDRAWN_DATE': 'Date of withdrawal',
    'ARRIVAL_DATE': 'Arrival Date',
    'FIRST_NAME': 'First Name',
    'LAST_NAME': 'Last Name',
    'DATE_OF_BIRTH': 'Birth Date',
    'PHONE_NUMBER': 'Primary Phone Number',
    'ISOLATION_PHONE_NUMBER': 'Phone Number at Isolation Location',
    'EXEMPT': 'Exempt'
};

export enum FLAG {
    ABTT_REQUIRED = 'ab_trace_required'
}

export enum SURVEY_QUESTIONNARIE_TYPE {
    DAY_4 = "DAY_4",
    DAY_13 = "DAY_13"
}

export enum WITHDRAWN_REASON {
    NON_COMPLIANCE = 'non-compliance',
    DEPARTED_COUNTRY = 'departed country',
    AHS_REQUEST = 'as per AHS request',
    OPTED_OUT = 'opted out',
    OTHER = 'other'
}
