import { Injectable } from '@nestjs/common';
import { LoggerService } from '../../logs/logger';
import { ActivityDTO } from '../../service-alberta/dto/activity.dto';
import { RTOPDbService } from '../../db/rtop-dbservice';
import { EnrollmentSearchQueryBuilder } from './enrollment-search-query-builder';
import { EnrollmentFormRO } from '../ro/enrollment-form.ro';
import { EditFormDTO } from '../dto/edit-form.dto';
import { EnrollmentFormSearchResultRO } from '../ro/enrollment-form-search.ro';
import { RtopTravellerSearchRO } from '../ro/rtop-traveller.ro';
import { Household, EnrollmentStatus } from '../entities/household.entity';
import { RTOPTraveller } from '../entities/rtop-traveller.entity';
import { DailySubmission } from '../entities/daily-submission.entity';
import { ReminderIDRO } from '../ro/reminder-id.ro';
import { HouseholdTravellersQueryStatusRO } from '../ro/household-submission-status.ro';
import { DailyMonitoringFormQuery } from '../../rtop-admin/dto/form-query.dto';
import { FLAG, WITHDRAWN_REASON } from '../constants';
import { DailyQuestionAnswerService } from '../daily-question-answer.service';
import { chunks } from '../../utils/chunks';
import { FeatureFlagService } from '../feature-flag.service';
import { TravellerQuery } from './traveller.query';
import { HouseholdQuery } from './enrollment-form.query';

@Injectable()
export class EnrollmentFormRepository {

    /**
     * Create traveller records and assign them to the given household
     * @param household household to assign travellers to
     * @param records travellers to create
     */
    async createTravellerRecords(household: Household, records: RTOPTraveller[]) {
        return await RTOPDbService.createTravellerRecords(household, records);
    }

    /**
     *
     * Counts the number of records with the corresponding confirmation number
     * This function is used to verify that a record exists
     *
     * @param code - confirmation number
     *
     * @returns The number of records with a matching confirmation number
     */
    async countByConfirmationNumber(code: string): Promise<number> {
        return await RTOPDbService.countByNum(code);
    }

    /**
     * Counts the number of households in the database
     *
     * @returns - The number of matching records
     */
    async count(): Promise<number> {
        return await RTOPDbService.getCount('household');
    }

    /**
     * Updates the quarantineLocation of a form with the given values
     *
     * @param agentId - ID of the agent making the change
     * @param arrivalFormId - The enrollment form id to update
     * @param updatedFields - The new quarantine location values
     */
    async editForm(agentId: string, arrivalForm: EnrollmentFormRO, updatedFields: EditFormDTO): Promise<void> {
        try {
            const updateRequest = JSON.stringify(
                {"$set":
                    {
                        hasPlaceToStayForQuarantine: updatedFields.hasPlaceToStayForQuarantine,
                        quarantineLocation: updatedFields.hasPlaceToStayForQuarantine? updatedFields.quarantineLocation: undefined
                    }
                }
            );

            // Actually update form with new values
            await RTOPDbService.editForm(agentId, arrivalForm.id, updateRequest);
        } catch(e) {
            LoggerService.error(`Failed to edit form with id ${arrivalForm.id}`, e);
            throw e;
        }
    }

    /**
     *
     * Find an enrollment form by id
     *
     * @param id - Id of the record
     *
     * @returns Enrollment form
     */
    async findOne(id: number): Promise<EnrollmentFormRO> {
        try {
            const res = await (await RTOPDbService.getForm(id)).single();
            return res;
        } catch (e){
            LoggerService.error('Failed to find form', e);

            throw e;
        }
    }

    /**
     * Find a record by confirmation number
     *
     * @param confirmationNumber
     *
     * @returns - enrollment form with the matching confirmation number
     */
    async findByConfirmationNumber(confirmationNumber: string): Promise<EnrollmentFormRO> {
        const res = await(await RTOPDbService.getFormByNumber(confirmationNumber))
            .single();
        return res;
    }

    /**
     * Find a record by a travellers confirmation number
     *
     * @param confirmationNumber confirmation number
     *
     * @returns - enrollment form that has a traveller with the matching confirmation number
     */
    async findByTravellerConfirmationNumber(confirmationNumber: string): Promise<EnrollmentFormRO> {
        const res = await(await TravellerQuery.getFormByTravellerConfirmationNumber(confirmationNumber))
            .single();
        return res;
    }

    /**
     * Find all enrollment forms that have a traveller with the corresponding last name.
     *
     * @param id - travellers id
     * @param birthdate - travellers date of birth
     *
     * @returns - traveller
     */
    async verifyByBirthDate(confirmationNumber: string, reminderToken: string, birthDate: string): Promise<RtopTravellerSearchRO> {
        if (!confirmationNumber || !reminderToken || !birthDate) { return null }

        const res = await(await RTOPDbService.verifyByBirthDate(confirmationNumber, reminderToken, birthDate));
        return res && new RtopTravellerSearchRO(res);
    }

    /**
     * Find all enrollment forms that have a traveller with the corresponding token.
     *
     * @param token - daily reminder id
     *
     * @returns - An array of travellers of a household.
     */
    async findByToken(token: string): Promise<EnrollmentFormSearchResultRO> {
        if (!token) { return new EnrollmentFormSearchResultRO('', [], false, null); }

        const travellers = await (await TravellerQuery.getFormByToken(token))
            .multipleTraveller();

        if (!travellers || travellers.length == 0) { return new EnrollmentFormSearchResultRO('', [], false, null); }

        const abttRequired = await this.isABTTMandatoryFlaggedForHousehold(travellers[0]?.householdId);
        const numberOfDailyCheckInsReceived = await this.getNumberOfDailyCheckInsReceived(travellers[0]?.householdId);

        const surveyQuestionnaire = await this.getSurveyQuestionnaireForHousehold(travellers[0]?.householdId);
        const result = new EnrollmentFormSearchResultRO(travellers[0]?.date, travellers, abttRequired, surveyQuestionnaire, true, numberOfDailyCheckInsReceived);
        if (result.travellerSurvey) {
            result.travellerSurvey = (await FeatureFlagService.isFeatureEnabled(result.travellerSurvey)) ? result.travellerSurvey : null;
        }
        return result;
    }

    /**
     * Get a reason for status change
     *
     * @param confirmationNumber - travellers confirmation number
     *
     * @returns - string value for reason
     */
    async getStatusReason(confirmationNumber: string): Promise<string> {
        if (!confirmationNumber) { return null }
        return await RTOPDbService.getStatusReason(confirmationNumber);
    }

    /**
     * Submit daily questionnaire for traveller
     *
     * @param submission - submitted answers
     * @param travellerId - confirmation number
     * @param reminderId - reminder id
     */
    async createDailySubmission(submission: DailySubmission, reminder: ReminderIDRO): Promise<any> {
        const cardStatus = DailyQuestionAnswerService.submissionCardStatus(submission);

        const encodedAnswers = DailyQuestionAnswerService.encodeAnswers(submission, reminder);
        const jsonSubmission = JSON.stringify(Object.assign({}, submission, {answers: encodedAnswers}));
        return await RTOPDbService.createDailySubmission(cardStatus.STATUS, jsonSubmission, reminder.travellerId, reminder.id);
    }

    /**
     * Updates the last screened timestamp for the given household and traveller
     * @param householdId ID of household to update
     * @param date last screened date
     */
    async updateLastScreened(householdId: number, travellerId: number, date: string) {
        await HouseholdQuery.updateLastScreened(householdId, date);
        await TravellerQuery.updateLastScreened(travellerId, date);
    }

    /**
     * Returns reminder id
     *
     * @param travellerId - confirmation number
     */
    async getReminderId(confirmationNumber: string, token: string): Promise<ReminderIDRO> {
        const res = await(await RTOPDbService.getReminderId(confirmationNumber, token));
        return res && new ReminderIDRO(res.value()[0], token);
    }

    /**
     * Saves an enrollment form
     *
     * @param form - form to save
     *
     * @returns - The enrollment form
     */
    async save(form: Household): Promise<Household> {
        const id:number = await(await RTOPDbService.saveForm(form)).idOnly();

        form.id = id;

        return form;
    }

    /**
     * Search households based on the given search filters.
     * 
     * This powers the list view of the monitoring portal
     * @param user user performing the search
     * @param query query to filter records
     */
    async search(user:any, query: DailyMonitoringFormQuery):Promise<any>{
        return new EnrollmentSearchQueryBuilder(user)
            .filter(query.filter)
            .agent(query.agent)
            .fromDate(query.fromDate)
            .filterCardStatus(query.cardStatus)
            .enrollmentStatus(query.enrollmentStatus)
            .toDate(query.toDate)
            .query(query.query)
            .orderBy(query.orderBy, query.order)
            .paginate(query.page, 20)
            .execute();
    }

    /**
     * Add a new activity to the given household 
     * @param activity activity to add 
     * @param userId user that added the activity
     * @param householdId id of household to add activity to
     */
    async addActivity(activity: ActivityDTO, userId: string, householdId: number):Promise<void>{
        const activityId: number = await HouseholdQuery.addActivity(activity, userId, householdId);
        await HouseholdQuery.updateLastActivity(householdId, activityId);
    }

    /**
     * Update the status of a household (1st call made etc)
     * @param status status enum to update to
     * @param householdId id of household to update status of
     */
    async updateStatus(status:string, householdId:number):Promise<boolean>{
        try{
            await RTOPDbService.updateStatus(status, householdId);
            return true;
        }catch(err){
            return false;
        }
    }

    /**
     * Update the status of a household (red / green / yellow)
     * @param confirmationNumber confirmation number of traveller to update
     * @param dailyStatus new status
     * @param reason reason for updating the status
     */
    async updateHouseholdCardStatus(confirmationNumber: string[], dailyStatus: string, reason: string, dailySubmission: number):Promise<any> {
        const cnChunks = chunks(confirmationNumber, 5000);
        let res = [];
        
        for(const ch of cnChunks) {
            await RTOPDbService.createHouseholdStatusHistory(ch, dailyStatus, reason, dailySubmission);
            const r = await RTOPDbService.updateHouseholdCardStatus(ch, dailyStatus, reason);
            res = res.concat(r);
        }

        return res;
        
    }

    /**
     * Override the status of a household from (red / yellow) to green by agent
     * @param confirmationNumber confirmation number of household to update
     * @param dailyStatus new status
     * @param reason reason for updating the status
     */
    async overrideHouseholdCardStatus(confirmationNumber: string, dailyStatus: string, reason: string):Promise<any> {
        return await RTOPDbService.overrideHouseholdCardStatus(confirmationNumber, dailyStatus, reason);
    }

    /**
     * Retrieve status of traveller based on their confirmation
     * @param confirmationNumber confirmation number of traveller to retrieve
     */
    async getTravellerStatusByConfirmationNumber(confirmationNumber: string): Promise<HouseholdTravellersQueryStatusRO> {
        const res = await RTOPDbService.getTravellerStatusForHousehold(confirmationNumber);
        return res;
    }

    /**
     * Update the status of a traveller (red / green / yellow)
     * @param travellerIds Traveller IDs
     * @param dailyStatus new status
     *
     */
    async updateTravellerCardStatus(travellerIds: number[], dailyStatus: string):Promise<any> {

        const travellerChunks = chunks(travellerIds, 5000);
        let res = [];
        
        for(const ch of travellerChunks) {
            res = res.concat(await RTOPDbService.updateTravellerCardStatus(ch, dailyStatus));
        }

        return res;
    }

    async updateDOBVerificationAttempts(confirmationNumber: string, isVerified: boolean): Promise<any> {
        if (isVerified) {
            // reset DOB verification attempt status
            return await RTOPDbService.resetDOBVerificationAttempts(confirmationNumber);
        }
        else {
            return await RTOPDbService.updateDOBVerificationAttempts(confirmationNumber);
        }
    }

    async makeABTTMandatoryFlagHousehold(householdId: number) {
        try {
            await RTOPDbService.enableFlagForHousehold(householdId, FLAG.ABTT_REQUIRED);
        } catch (error) {
            LoggerService.error(`Error when flagging household - ${householdId} for ABTT`, error)
        }
    }

    async isABTTMandatoryFlaggedForHousehold(householdId: number): Promise<boolean> {
        try {
            const flagProps = await RTOPDbService.getFlagPropForHousehold(householdId, FLAG.ABTT_REQUIRED);
            return flagProps[0]?.ENABLED;
        } catch (error) {
            LoggerService.error(`Error when fetching flag for household - ${householdId}`, error)
        }
        return false;
    }
    async getSurveyQuestionnaireForHousehold(householdId: number) {
        return await RTOPDbService.getSurveyQuestionnarieForHousehold(householdId);
    }

    async submitSurveyQuestionnaire(submission: DailySubmission, householdId: number, surveyType: string): Promise<any> {
        const jsonSubmission = JSON.stringify(submission);

        return await RTOPDbService.createSurveyQuestionnaire(jsonSubmission, householdId, surveyType);
    }

    
    async updatePrimaryTravellerHousehold(household: any, formattedHouseholdEntity: any, householdId: number) {
        const updateRequest = JSON.stringify(
            {
                "$set": household
            }
        );

        await TravellerQuery.updateHouseholdPrimaryTraveller(
            updateRequest,
            formattedHouseholdEntity,
            householdId
        );
    }

    async updateTravellerRecords(record: any, confirmationNumber: string) {
        return await TravellerQuery.updateTravellerRecord(record, confirmationNumber);
    }

    async updateAdditionalTravellersInformation(additionalTravellers: any[], householdId: number) {
        const updatedFields = JSON.stringify(
            {
                "$set":
                {
                    additionalTravellers
                }
            });

        await TravellerQuery.updateHouseholdAdditionalTravellers(updatedFields, householdId);
    }

    async updateHouseholdContactMethod(contactRecord: any, householdId: number) {
        await TravellerQuery.updateHouseholdContactMethod(contactRecord, householdId);
    }

    /**
     * 
     * Updates enrollment status of the household, and inserts data to history table as well
     * 
     */
    async updateHouseholdEnrollmentStatus(enrollmentStatus: EnrollmentStatus, householdId: number, withdrawalReason?: WITHDRAWN_REASON): Promise<void> {
        let updateCount;
        if (enrollmentStatus === EnrollmentStatus.ENROLLED) {
            updateCount = await TravellerQuery.updateHouseholdEnrolledStatus(householdId);
        } else if (enrollmentStatus === EnrollmentStatus.WITHDRAWN) {
            updateCount = await TravellerQuery.updateHouseholdWithdrawnStatus(householdId, withdrawalReason);
        }

        if (updateCount[0][1] > 0) {
            await TravellerQuery.addHouseholdEnrollmentStatusHistory(householdId, enrollmentStatus, withdrawalReason);

            if( enrollmentStatus === EnrollmentStatus.ENROLLED )
            {
                await this.makeABTTMandatoryFlagHousehold(householdId);
            }
        }
    }

    /**
     * 
     * Updates the traveller's enrollment status, and inserts the same in the history table
     * 
     */
    async updateTravellerEnrollmentStatus(enrollmentStatus: EnrollmentStatus, travellerConfirmationNumber: string, withdrawalReason?: WITHDRAWN_REASON): Promise<void> {
        let travellerIds;
        if (enrollmentStatus === EnrollmentStatus.ENROLLED) {
            travellerIds = await TravellerQuery.updateTravellerEnrolledStatus(travellerConfirmationNumber);
        } else if (enrollmentStatus === EnrollmentStatus.WITHDRAWN) {
            travellerIds = await TravellerQuery.updateTravellerWithdrawnStatus(travellerConfirmationNumber, withdrawalReason);
        }

        if (!travellerIds || travellerIds.length < 1) {
            return;
        }
        travellerIds = travellerIds.map((t) => t.ID);
        await TravellerQuery.addTravellerEnrollmentStatusHistory(travellerIds, enrollmentStatus, withdrawalReason);
    }

    /**
     * 
     * Backfills household status with respect to the traveller's status, 
     * and inserts the same in the history table
     * 
     */
    async backfillHouseholdEnrollmentStatus(enrollmentStatus: EnrollmentStatus, householdId: number, withdrawalReason?: WITHDRAWN_REASON): Promise<void> {
        let updateCount;
        if (enrollmentStatus === EnrollmentStatus.ENROLLED) {
            updateCount = await TravellerQuery.updateHouseholdEnrolledStatus(householdId);
        } else if (enrollmentStatus === EnrollmentStatus.WITHDRAWN) {
            updateCount = await TravellerQuery.backfillHouseholdWithdrawnStatus(householdId, withdrawalReason);
        }

        if (updateCount[0][1] > 0) {
            await TravellerQuery.addHouseholdEnrollmentStatusHistory(householdId, enrollmentStatus, withdrawalReason);

            if( enrollmentStatus === EnrollmentStatus.ENROLLED )
            {
                await this.makeABTTMandatoryFlagHousehold(householdId);
            }
        }
    }

    /**
     * 
     * Backfills all the traveller's status in the household,
     * and inserts the same in the history table
     * 
     */
    async backfillTravellerEnrollmentStatus(enrollmentStatus: EnrollmentStatus, householdId: number, withdrawalReason?: WITHDRAWN_REASON): Promise<void> {
        let travellerIds;
        if (enrollmentStatus === EnrollmentStatus.ENROLLED) {
            travellerIds = await TravellerQuery.backfillTravellerEnrolledStatus(householdId);
        } else if (enrollmentStatus === EnrollmentStatus.WITHDRAWN) {
            travellerIds = await TravellerQuery.backfillTravellerWithdrawnStatus(householdId, withdrawalReason);
        }

        if (!travellerIds || travellerIds.length < 1) {
            return;
        }
        travellerIds = travellerIds.map((t) => t.ID);
        await TravellerQuery.addTravellerEnrollmentStatusHistory(travellerIds, enrollmentStatus, withdrawalReason);
    }

    async getNumberOfDailyCheckInsReceived(householdId: number) {
        try {
            const count = await RTOPDbService.getNumberOfDailyCheckIns(householdId);
            return count[0][1];
        } catch (error) {
            LoggerService.error(`Error fetching count of number of daily checkins for household - ${householdId}`, error)
        }
        return 0;
    }
}
