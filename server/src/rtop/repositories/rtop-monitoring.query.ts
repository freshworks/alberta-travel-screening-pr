import { Injectable } from "@nestjs/common";
import { query } from "../../db/db2connection";
import { enrollmentFormQuery, travellerStatusQuery } from "../../db/rtop-db2connectionUtils";
import { EnrollmentStatus } from "../entities/household.entity";


@Injectable()
// TODO: all the queries are for the arrival form -> kindly change them accordingly
export class RTOPMonitoringQuery {

    private static getEnrolledTravellersQuery = (hasDate) => `
    select t.CONFIRMATION_NUMBER,
        SYSTOOLS.BSON2JSON(h.form_record) as form_record,
        t.enrollment_status,
        t.DETERMINATION_DATE,
        t.CARD_STATUS,
        t.CARD_STATUS_TIME,
        h.ARRIVAL_DATE
    from household as h
    join rtop_traveller t on t.household_id=h.id
    where (t.ENROLLMENT_STATUS = '${EnrollmentStatus.ENROLLED}' or t.ENROLLMENT_STATUS = '${EnrollmentStatus.COMPLETED}')
        ${hasDate ? " AND t.DETERMINATION_DATE BETWEEN ? AND ?" : ""} ORDER BY t.DETERMINATION_DATE ASC
    `;

    private static readonly getWithdrawnTravellersQuery = (hasDate) => `
    select t.CONFIRMATION_NUMBER,
        SYSTOOLS.BSON2JSON(h.form_record) as form_record,
        t.enrollment_status,
        t.DETERMINATION_DATE,
        t.CARD_STATUS ,
        t.CARD_STATUS_TIME,
        h.ARRIVAL_DATE,
        t.WITHDRAWN_DATE
    from household as h
    join rtop_traveller t on t.household_id=h.id
    where t.ENROLLMENT_STATUS = '${EnrollmentStatus.WITHDRAWN}' 
        ${hasDate ? " AND t.WITHDRAWN_DATE BETWEEN ? AND ?" : ""} ORDER BY t.WITHDRAWN_DATE ASC
    `;

    private static readonly GetEnrolledTravellersReportCount = `
    SELECT
	    TRIM(BOTH FROM JSON_VAL(h.form_record, 'nameOfAirportOrBorderCrossing', 's:200')) as nameOfAirportOrBorderCrossing,
	    count(*) as count
        FROM RTOP_TRAVELLER rt 
        JOIN household h ON h.id=rt.HOUSEHOLD_ID
        WHERE rt.DETERMINATION_DATE BETWEEN ? AND ? GROUP BY
	        TRIM(BOTH FROM JSON_VAL(h.form_record, 'nameOfAirportOrBorderCrossing', 's:200'));
    `;

    
    private static readonly ExportEnrolledTravellersReport = `
    SELECT
        rt.DETERMINATION_DATE AS ENROLLMENT_DATE,
        rt.FIRST_NAME, 
        rt.LAST_NAME, 
        rt.BIRTH_DATE AS DATE_OF_BIRTH,
        h.ARRIVAL_DATE, 
        COALESCE(NULLIF(h.CONTACT_PHONE_NUMBER, ''), NULLIF(h.PHONE_NUMBER, ''), JSON_VAL(h.form_record, 'phoneNumber', 's:200')) AS PHONE_NUMBER,
        JSON_VAL(h.form_record, 'quarantineLocationPhoneNumber' , 's:200' ) as ISOLATION_PHONE_NUMBER,
        JSON_VAL(h.form_record, 'exemptionType', 's:200') as EXEMPT
    FROM RTOP_TRAVELLER rt
        INNER JOIN HOUSEHOLD h ON h.ID = rt.HOUSEHOLD_ID 
    WHERE rt.DETERMINATION_DATE BETWEEN ? AND ?;
    `;

    private static readonly ExportWithdrawnTravellersReport = `
    SELECT
        rt.DETERMINATION_DATE AS ENROLLMENT_DATE,
        rt.WITHDRAWN_DATE AS WITHDRAWN_DATE,
        h.ARRIVAL_DATE,
        rt.FIRST_NAME,
        rt.LAST_NAME,
        rt.BIRTH_DATE AS DATE_OF_BIRTH, 
        COALESCE(NULLIF(h.CONTACT_PHONE_NUMBER, ''), NULLIF(h.PHONE_NUMBER, ''), JSON_VAL(h.form_record, 'phoneNumber', 's:200')) AS PHONE_NUMBER,
        JSON_VAL(h.form_record, 'quarantineLocationPhoneNumber' , 's:200' ) as ISOLATION_PHONE_NUMBER,
        JSON_VAL(h.form_record, 'exemptionType', 's:200') as EXEMPT
        FROM RTOP_TRAVELLER rt INNER JOIN HOUSEHOLD h ON h.ID = rt.HOUSEHOLD_ID 
        WHERE rt.WITHDRAWN_DATE BETWEEN ? AND ?;
    `;


    public static async getEnrolledTravellerStatusForDate(fromDate: string, toDate: string) {
        return await travellerStatusQuery(RTOPMonitoringQuery.getEnrolledTravellersQuery(true), [fromDate, toDate]);
    }

    public static async getWithdrawnTravellerStatusForDate(fromDate: string, toDate: string) {
        return await travellerStatusQuery(RTOPMonitoringQuery.getWithdrawnTravellersQuery(true), [fromDate, toDate]);
    }

    public static async getEnrolledTravellerStatusAfterDate(fromDate: string) {
        return await travellerStatusQuery(RTOPMonitoringQuery.getEnrolledTravellersQuery(true), [fromDate]);
    }

    public static async getEnrolledTravellerStatus() {
        return await travellerStatusQuery(RTOPMonitoringQuery.getEnrolledTravellersQuery(false), []);
    }

    public static async getWithdrawnTravellerStatusAfterDate(fromDate: string) {
        return await travellerStatusQuery(RTOPMonitoringQuery.getWithdrawnTravellersQuery(true), [fromDate]);
    }

    public static async getWithdrawnTravellerStatus() {
        return await travellerStatusQuery(RTOPMonitoringQuery.getWithdrawnTravellersQuery(false), []);
    }

    public static async getEnrolledTravellersCount(fromDate: string, toDate: string) {
        return query(RTOPMonitoringQuery.GetEnrolledTravellersReportCount, [fromDate, toDate]);
    }

    public static async getEnrolledTravellersReport(fromDate: string, toDate: string) {
        return query(RTOPMonitoringQuery.ExportEnrolledTravellersReport, [fromDate, toDate]);
    }

    public static async getWithdrawnTravellersReport(fromDate: string, toDate: string) {
        return query(RTOPMonitoringQuery.ExportWithdrawnTravellersReport, [fromDate, toDate]);
    }
}