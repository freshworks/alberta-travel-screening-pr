import {
    IsArray,
    ValidateNested,
    IsOptional,
    IsString,
    Length,
    IsIn,
} from 'class-validator';

import { Exclude, Type } from 'class-transformer';
import { DailySubmissionAnswer } from '../enrollment-form.constants';
import { SURVEY_QUESTIONNARIE_TYPE } from '../constants';

export class SurveyQuestionnaireDTO {

    @IsArray()
    @ValidateNested({ each: true })
    @Type(() => DailySubmissionAnswer)
    answers: Array<DailySubmissionAnswer>;


    @IsString()
    @IsIn(Object.values(SURVEY_QUESTIONNARIE_TYPE))
    surveyType: string;
}
