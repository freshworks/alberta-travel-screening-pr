import { camelCaseObjectKeys } from "../../utils/camelCaseObjectKeys"


describe('camelCaseObjectKeys', () => {
    it('should lowercase strings', () => {
        const res = camelCaseObjectKeys({
            TEST: 'abc',
            ANOTHERTEST: 'cdef'
        });

        expect(res).toEqual({
            test: 'abc',
            anothertest: 'cdef'
        });
    });
    it('should replace underscores', () => {
        const res = camelCaseObjectKeys({
            TEST: 'abc',
            ANOTHER_TEST: 'cdef',
            ANOTHER_TEST_TEST: 'cdef'
        });

        expect(res).toEqual({
            test: 'abc',
            anotherTest: 'cdef',
            anotherTestTest: 'cdef'
        });
    });
})