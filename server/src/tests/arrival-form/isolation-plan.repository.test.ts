import { createModule } from "../util/db-test-utils";
import { IsolationPlanReportRepository, IsolationPlanReportProperty, DuplicatePlanReportError } from "../../arrival-form/repositories/isolation-plan.repository";
import { ClearDB } from "../../db/db2migrations";
import { envCond } from "../util/test-utils";


describe('findByLastName', () => {
  let repository: IsolationPlanReportRepository;
  let module;

  beforeAll(async () => {
    await ClearDB();
  });

  beforeEach(async () => {
    module = await createModule({ entities: [IsolationPlanReportRepository], providers: [IsolationPlanReportRepository], imports: [] });
    repository = module.module.get(IsolationPlanReportRepository);
  });

  afterEach(async () => {
    await module.close();
  });

  const validPlan = {
    date: '2010/01/02',
    failed: 1,
    passed: 0,
    failedThenPassed: 0,
    passedThenFailed: 0
  };

  const validPlan2 = {
    date: '2010/01/03',
    failed: 1,
    passed: 0,
    failedThenPassed: 0,
    passedThenFailed: 0
  };

  describe('save', () => {
    envCond('TRAVELLER')('should save entity with passed in values', async () => {
      const id = await repository.save(validPlan);
      const entity = await repository.findOne(id);

      expect(entity).toEqual({
        id,
        ...validPlan
      });

      expect(await repository.count()).toEqual(1);
    });

    envCond('TRAVELLER')('saves one entry per date only', async () => {
      await repository.save(validPlan);

      try {
        await repository.save(validPlan);
        fail('Did not throw DuplicatePlanReportError');
      } catch (e) {
        expect(e).toBeInstanceOf(DuplicatePlanReportError);
      }
    });
    envCond('TRAVELLER')('saves entries when date differs', async () => {
      await repository.save(validPlan);
      await repository.save(validPlan2);

      expect(await repository.count()).toEqual(2);
    });

  });

  describe('findByDate', () => {
    envCond('TRAVELLER')('returns entity when match is found', async () => {
      const id = await repository.save(validPlan);
      const entity = await repository.findByDate(validPlan.date);

      expect(entity).toEqual({
        id,
        ...validPlan
      });
    });

    envCond('TRAVELLER')('returns null when no match is found', async () => {
      await repository.save(validPlan);
      const entity = await repository.findByDate('2020/01/01');

      expect(entity).toBeNull();
    });
  });

  describe('increment', () => {
    let res;
    envCond('TRAVELLER')('increments passed in property when called once', async () => {
      const id = await repository.save(validPlan);
      await repository.increment(id, IsolationPlanReportProperty.FAILED_THEN_PASSED);
      const entity = await repository.findOne(id);

      expect(entity.failedThenPassed).toEqual(1);
    });

    envCond('TRAVELLER')('increments passed in property when called multiple times', async () => {
      const id = await repository.save(validPlan);

      await repository.increment(id, IsolationPlanReportProperty.PASSED_THEN_FAILED);
      await repository.increment(id, IsolationPlanReportProperty.PASSED_THEN_FAILED);
      await repository.increment(id, IsolationPlanReportProperty.PASSED_THEN_FAILED);

      const entity = await repository.findOne(id);

      expect(entity.passedThenFailed).toEqual(3);
    });

    envCond('TRAVELLER')('increments multiple properties', async () => {
      const id = await repository.save(validPlan);

      await repository.increment(id, IsolationPlanReportProperty.PASSED_THEN_FAILED);
      await repository.increment(id, IsolationPlanReportProperty.PASSED);
      await repository.increment(id, IsolationPlanReportProperty.PASSED);
      await repository.increment(id, IsolationPlanReportProperty.FAILED);

      const entity = await repository.findByDate(validPlan.date);

      expect(entity.passedThenFailed).toEqual(1);
      expect(entity.passed).toEqual(2);
      expect(entity.failed).toEqual(2);
    });

  });
});
