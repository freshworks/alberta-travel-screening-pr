import { Injectable } from '@nestjs/common';
import { WebClient } from '@slack/web-api';
import { LoggerService } from './logs/logger';

@Injectable()
export class SlackNotifierService {
    private token;
    private web;
    private conversationId: string;

    constructor() {
        this.token = process.env.SLACK_TOKEN;
        this.web = new WebClient(this.token);
        this.conversationId = process.env.SLACK_CHANNEL_ID;
    }

    async postMessage(message: string): Promise<any> {
        try {
            await this.web.chat.postMessage({
                text: message,
                channel: this.conversationId,
            });
        }
        catch (e) {
            LoggerService.error("Error sending notifications via slack", e);
        }
    }
}