export enum PerformedTestImportError {
    INVALID_CONFIRMATION_NUMBER = 'invalid_confirmation_number',
    INVALID_DATE = 'invalid_date'
}

export class PerformedTestRecordRO {
    confirmationNumber: string;
    failureReason: PerformedTestImportError;

    constructor(confirmationNumber: string, failureReason: PerformedTestImportError) {
        this.confirmationNumber = confirmationNumber;
        this.failureReason = failureReason;
    }
}

export class PerformedTestsRecordsRO {
    errors: PerformedTestRecordRO[];

    constructor(errors: PerformedTestRecordRO[]) {
        this.errors = errors;
    }
}