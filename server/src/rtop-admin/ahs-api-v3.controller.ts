import {
  Controller,
  Param,
  UseInterceptors,
  Get,
  Req,
  HttpException,
  HttpStatus,
  ClassSerializerInterceptor,
  UseGuards,
  Patch,
  Body,
} from '@nestjs/common';
import { EnrollmentFormService } from '../rtop/enrollment-form.service';
import { PublicRoute } from '../decorators';
import { AuthGuard } from '@nestjs/passport';
import { LoggerService } from '../logs/logger';
import { ActionName } from '../logs/enums';
import { EnrollmentFormRepository } from '../rtop/repositories/enrollment-form.repository';
import { EnrollmentStatus } from '../rtop/entities/household.entity';
import { EnrollmentFormDTO } from '../rtop/dto/enrollment-form.dto';
import { HouseholdTraveller } from './dto/household-traveller.dto';
import { WITHDRAWN_REASON } from '../rtop/constants';

const throw404 = (message = 'No entries found') => {
  throw new HttpException({
    status: HttpStatus.NOT_FOUND,
    error: message,
  }, HttpStatus.NOT_FOUND);
}

@UseGuards(AuthGuard('jwt'))
@Controller('/api/v3/rtop/ahs')
export class AHSApiV3Controller {

  constructor(
    private readonly enrollmentFormService: EnrollmentFormService,
    private readonly enrollmentFormRepository: EnrollmentFormRepository) { }

  // Disable global guards
  @PublicRoute()
  @UseInterceptors(ClassSerializerInterceptor)
  @Get('traveller/:confirmationNumber')
  async findTraveller(@Req() req, @Param('confirmationNumber') confirmationNumber: string): Promise<any> {
    // Look up traveller
    const enrolment: any = await this.enrollmentFormService.findByTravellerConfirmationNumber(confirmationNumber);

    if (!enrolment) throw404('Traveller with matching confirmation number not found.')

    LoggerService.logData(req, ActionName.VIEW, confirmationNumber, [enrolment.id]);

    return enrolment;
  }

  @PublicRoute()
  @UseInterceptors(ClassSerializerInterceptor)
  @Patch('traveller/:confirmationNumber/enroll')
  async enrollTraveller(@Req() req, @Param('confirmationNumber') confirmationNumber: string): Promise<void> {
    // Look up traveller
    const household: any = await this.enrollmentFormService.findByTravellerConfirmationNumber(confirmationNumber);

    if (!household) throw404('Traveller with matching confirmation number not found.')

    LoggerService.logData(req, ActionName.AHS_ENROLL_TRAVELLER, confirmationNumber, [household.id]);

    await this.enrollmentFormService.updateTravellerEnrollmentStatus(confirmationNumber, household.id, EnrollmentStatus.ENROLLED);
  }

  @PublicRoute()
  @UseInterceptors(ClassSerializerInterceptor)
  @Patch('traveller/:confirmationNumber/withdraw')
  async withdrawTraveller(@Req() req, @Param('confirmationNumber') confirmationNumber: string): Promise<void> {
    // Look up traveller
    const household: any = await this.enrollmentFormService.findByTravellerConfirmationNumber(confirmationNumber);

    if (!household) throw404('Traveller with matching confirmation number not found.')

    LoggerService.logData(req, ActionName.AHS_WITHDRAW_TRAVELLER, confirmationNumber, [household.id]);

    await this.enrollmentFormService.updateTravellerEnrollmentStatus(confirmationNumber, household.id, EnrollmentStatus.WITHDRAWN, WITHDRAWN_REASON.OTHER);
  }

  @PublicRoute()
  @UseInterceptors(ClassSerializerInterceptor)
  @Patch('traveller/:confirmationNumber')
  async updateTraveller(@Req() req, @Param('confirmationNumber') confirmationNumber: string, @Body() body: HouseholdTraveller): Promise<void> {
    const household: any = await this.enrollmentFormRepository.findByTravellerConfirmationNumber(confirmationNumber);

    if (!household) throw404('Traveller with matching confirmation number not found.')

    if(!body || Object.keys(body).length == 0) throw404('No data provided.')
    confirmationNumber = confirmationNumber.toUpperCase();

    LoggerService.logData(req, ActionName.AHS_UPDATE_TRAVELLER, confirmationNumber, [household.id]);

    await this.enrollmentFormService.updateTravellerInformation(household, confirmationNumber, body);
  }

}