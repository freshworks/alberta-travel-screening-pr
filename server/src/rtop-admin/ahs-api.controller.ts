import {
    Controller,
    Param,
    UseInterceptors,
    Get,
    Req,
    HttpException,
    HttpStatus,
    ClassSerializerInterceptor,
    UseGuards,
  } from '@nestjs/common';
import { EnrollmentFormService } from '../rtop/enrollment-form.service';
import { PublicRoute } from '../decorators';
import { AuthGuard } from '@nestjs/passport';
import { LoggerService } from '../logs/logger';
import { ActionName } from '../logs/enums';
import { EnrollmentStatus } from '../rtop/entities/household.entity';
  
const throw404 = (message = 'No entries found') => {
  throw new HttpException({
    status: HttpStatus.NOT_FOUND,
    error: message,
  }, HttpStatus.NOT_FOUND);
}

@Controller('/api/v2/rtop/ahs')
export class AHSApiController {

  constructor(
    private readonly enrollmentFormService: EnrollmentFormService  ){}

  /**
   * Retrieves a traveller by the travellers confirmation number.
   * The retrieved traveller contains all the details of the household with `additionalTravellers`
   * omitted, and the primary travellers info replaced with the travellers.
   * 
   * Enrolls the household the traveller is part of into the program if not already done.
   * 
   * This endpoint is used to retrieve traveller info by AHS, at that point
   * the traveller has gone through (and passed) all screening checkpoints
   * so should be considered enrolled in the program.
   * 
   */
  // Disable global guards
  @PublicRoute()
  // Enable jwt auth guard
  @UseGuards(AuthGuard('jwt'))
  @UseInterceptors(ClassSerializerInterceptor)
  @Get('traveller/:confirmationNumber')
  async findAndEnrolTraveller(@Req() req, @Param('confirmationNumber') confirmationNumber: string): Promise<any>{
      // Look up traveller
      const household: any = await this.enrollmentFormService.findByTravellerConfirmationNumber(confirmationNumber);
      
      if (!household) throw404('Traveller with matching confirmation number not found.')
      
      // Enrol the household the traveller is part of if not already done
      await this.enrollmentFormService.updateTravellerEnrollmentStatus(confirmationNumber, household.id, EnrollmentStatus.ENROLLED);

      LoggerService.logData(req, ActionName.VIEW, confirmationNumber, [household.id]);

      household.enrollmentStatus = EnrollmentStatus.ENROLLED;
      return household;
  }
}
