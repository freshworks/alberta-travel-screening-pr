import { Logger, Injectable } from '@nestjs/common';

import twilio from 'twilio';
import { LoggerService } from './logs/logger';

export class TextSendable {
    to: string;
    message: string;
}

@Injectable()
export class TextService {
    private client;
    private outNumber: string;

    constructor() {
        let accountSid, authToken;

        if (process.env.VCAP_SERVICES) {
            // Provided by IBM Cloud
            const env = JSON.parse(process.env.VCAP_SERVICES);
            const local_creds = env['user-provided'][0].credentials;
            accountSid = local_creds.twilio_account_sid;
            authToken = local_creds.twilio_auth_token;
            this.outNumber = local_creds.twilio_phone_number;
        } else {
            // If not running in IBM Cloud
            this.outNumber = process.env.TWILIO_PHONE_NUMBER;
            accountSid = process.env.TWILIO_ACCOUNT_SID;
            authToken = process.env.TWILIO_AUTH_TOKEN;
        }
        
        this.client = twilio(accountSid, authToken);
    }

    /**
     * Sends a text message to the given number using Twilio
     */
    async sendText(text: TextSendable): Promise<void> {
        this.client.messages
            .create({
                to: text.to,
                from: this.outNumber,
                body: text.message,
            })
            .catch(e => {
                LoggerService.logger.error('Failed to send text', e);
            });
    }
}