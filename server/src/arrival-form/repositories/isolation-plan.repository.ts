import { Injectable } from '@nestjs/common';
import { DB2_ERRORS } from "../../db/db2errors";
import { LoggerService } from '../../logs/logger';
import { IsolationPlanReport } from '../entities/isolation-plan-report.entity';
import { DbService } from '../../db/dbservice';

export enum IsolationPlanReportProperty {
  PASSED = 'passed',
  FAILED = 'failed',
  PASSED_THEN_FAILED = 'passedThenFailed',
  FAILED_THEN_PASSED = 'failedThenPassed'
}

export class DuplicatePlanReportError extends Error {
  constructor(message: string) {
    super(message);

    Object.setPrototypeOf(this, DuplicatePlanReportError.prototype);
  }
}

@Injectable()
export class IsolationPlanReportRepository {

  /**
   * Saves
   *
   * @param plan - Isolation Plan report to be saved
   *
   * @returns - Returns the Id of the saved record
   */
  async save(plan: IsolationPlanReport): Promise<number> {
    try {
      const res = await DbService.saveReport(plan);
      const [{ ID = null }] = res || [];

      return ID;
    } catch (e) {
      LoggerService.error('Failed to save report', e);
      if (e.sqlcode === DB2_ERRORS.UNIQUE_CONSTRAINT) {
        throw new DuplicatePlanReportError('Failed to save report');
      } else {
        throw e;
      }
    }
  }

  /**
   * Create a report for a given day
   *
   * @param date - to create a report for
   *
   * @param prop - one of ['passed', 'failed']. This represents the result of the first arrival form inserted.
   */
  async createForDate(date: string, prop: IsolationPlanReportProperty): Promise<number> {
    const newReport:IsolationPlanReport ={
        id: undefined,
        date,
        passed: 0,
        failed: 0,
        failedThenPassed:0,
        passedThenFailed:0
    };

    newReport[prop] = 1;

    return this.save(newReport);
  }

  /**
   * When an an isolation plan is
   *
   * @param id - Id of the report to update
   * @param property - property to update (passedThenFailed/failedThenPassed)
   */
  async increment(id: number, property: IsolationPlanReportProperty): Promise<void> {
    try {
      await DbService.updateReport(id, property);
    } catch (e) {
      LoggerService.error('Failed to increment report', e);

      throw e;
    }
  }

  /**
   * Find an isolation plan report by ID.
   *
   * @param id - ID of the report
   *
   * @returns - Isolation plan report with matching ID
   */
  async findOne(id: number): Promise<IsolationPlanReport> {
    try {
      const res = await DbService.getReportById(id);
      const [report] = res || [null];

      return report ? new IsolationPlanReport(report) : null;
    } catch (e) {
      LoggerService.error('Failed find report', e);
      throw e;
    }
  }

  /**
   * Finds an isolation plan report for a given day
   *
   * @param date - The day - format
   *
   * @returns - Isolation Plan Report for the given day
   */
  async findByDate(date: string): Promise<IsolationPlanReport> {
    try {
      const res = await DbService.getReportByDate(date);
      const [report] = res || [null];
      return report ? new IsolationPlanReport(report) : null;
    } catch (e) {
      LoggerService.error('Failed to find report by date', e);
      throw e;
    }
  }

  /**
   * Counts the number of isolation plan reports
   *
   * @returns - Number of reports
   */
  async count(): Promise<number> {
    try {
      return DbService.getCount('report');
    } catch(e) {
      LoggerService.error('Failed to count reports', e);

      throw e;
    }
  }
}
