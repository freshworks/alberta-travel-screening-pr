import {
  Controller,
  Param,
  Body,
  UseInterceptors,
  Get,
  Patch,
  Req,
  HttpException,
  HttpStatus,
  ClassSerializerInterceptor,
  Post,
} from '@nestjs/common';
import { LoggerService } from '../logs/logger';
import { ActionName } from '../logs/enums';
import { ArrivalFormRepository } from './repositories/arrival-form.repository';
import { ArrivalFormService } from './arrival-form.service';
import { SubmissionDeterminationDTO } from "./dto/submission-determination.dto";
import { ArrivalFormSearchResultRO } from './ro/arrival-form-search.ro';
import { ArrivalFormRO } from './ro/arrival-form.ro';
import { IsString, MinLength } from 'class-validator';
import { Roles, PublicRoute } from '../decorators';
import { UnwillingTravellerDTO } from './dto/unwilling-traveller.dto';
import { UnwillingTravellerService } from './unwilling-traveller.service';
import { UnwillingTravellerRO } from './ro/unwilling-traveller.ro';
import { EditFormDTO } from './dto/edit-form.dto';
import { AgentDTO, ServiceAlbertaRepository } from '../service-alberta/service-alberta.repository';
import { Role, Scope } from '../auth/roles.enum';
import { DailyQuestionnaireService, JobTypes } from '../rtop-admin/daily-reminder.service';

const throw404 = (message = 'No entries found') => {
  throw new HttpException({
    status: HttpStatus.NOT_FOUND,
    error: message,
  }, HttpStatus.NOT_FOUND);
}


class SearchParams {
  @IsString()
  @MinLength(2)
  lname: string;
}

@Controller('/api/v1/admin/back-office')
export class ArrivalFormAdminController {

  constructor(
    private readonly arrivalFormRepository: ArrivalFormRepository,
    private readonly arrivalFormService: ArrivalFormService,
    private readonly unwillingTravellerService: UnwillingTravellerService,
    private readonly serviceAlbertaRepository: ServiceAlbertaRepository
  ){}

  @Roles('AHS', { ROLE: Role.OTHER, SCOPE: Scope.SCREENER })
  @UseInterceptors(ClassSerializerInterceptor)
  @Get('/lastname/:lname')
  async findByLastName(@Req() req, @Param() {lname}: SearchParams): Promise<ArrivalFormSearchResultRO> {
    if(!lname) {
      LoggerService.logData(req, ActionName.SEARCH, lname, null);

      throw404();
    }
    
    const res: ArrivalFormSearchResultRO = await this.arrivalFormRepository.findByLastName(lname);
    if (!res.travellers || !res.travellers.length) {
      LoggerService.logData(req, ActionName.SEARCH, lname, null);
      throw404();
    }

    LoggerService.logData(req, ActionName.SEARCH, lname, res.travellers.map(t => t.id));
    return res;
  }

  @Roles('AHS', { ROLE: Role.OTHER, SCOPE: Scope.SCREENER})
  @UseInterceptors(ClassSerializerInterceptor)
  @Get('form/:confirmationNumber')
  async findOne(@Req() req, @Param('confirmationNumber') confirmationNumber: string): Promise<ArrivalFormRO>{
      const arrivalForm: ArrivalFormRO = await this.arrivalFormRepository.findByConfirmationNumber(confirmationNumber);
      
      if (!arrivalForm) throw404('Form with matching confirmation number not found.')

      LoggerService.logData(req, ActionName.VIEW, confirmationNumber, [arrivalForm.id]);

      return arrivalForm;
  }

  @Roles('AHS',{ ROLE: Role.OTHER, SCOPE: Scope.SCREENER})
  @UseInterceptors(ClassSerializerInterceptor)
  @Patch('/form/:id/determination')
  async submitDetermination(@Req() req, @Param('id') id:number, @Body() determination: SubmissionDeterminationDTO): Promise<any> {
    try {
      const arrivalForm: ArrivalFormRO = await this.arrivalFormRepository.findOne(id);
      if (!arrivalForm) throw404('Form not found');

      const res = this.arrivalFormService.submitDetermination(arrivalForm, determination);
      LoggerService.logData(req, ActionName.UPDATE, `${arrivalForm.id}`, [arrivalForm.id]);
      return res;
    } catch(e) {
      LoggerService.error('Failed to submit determination', e);

      throw e;
    }
  }

  @Roles('AHS',{ ROLE: Role.OTHER, SCOPE: Scope.SCREENER})
  @UseInterceptors(ClassSerializerInterceptor)
  @Patch('/form/:id')
  async editForm(@Req() req, @Param('id') id:number, @Body() updatedFields: EditFormDTO): Promise<void> {
    try {
      const arrivalForm: ArrivalFormRO = await this.arrivalFormRepository.findOne(id);
      if (!arrivalForm) throw404('Form not found');
      const agent: AgentDTO = await this.serviceAlbertaRepository.getOrCreateAgent(req.user);

      const updatedFieldNames = await this.arrivalFormService.editForm(agent, arrivalForm, updatedFields);
      LoggerService.logData(req, ActionName.EDIT, updatedFieldNames.join(',') || 'No fields updated', [arrivalForm.id]);
    } catch(e) {
      LoggerService.error('Failed to edit form', e);

      throw e;
    }
  }

  @Roles('AHS',{ ROLE: Role.OTHER, SCOPE: Scope.SCREENER})
  @UseInterceptors(ClassSerializerInterceptor)
  @Post('/unwilling-traveller')
  async recordUnwillingTraveller(@Req() req, @Body() unwillingTraveller: UnwillingTravellerDTO): Promise<UnwillingTravellerRO> {
    try {
      const record: UnwillingTravellerRO = await this.unwillingTravellerService.recordUnwillingTraveller(req, unwillingTraveller);
      LoggerService.logData(req, ActionName.RECORD_UNWILLING_TRAVELLER, null, [record.id]);
      return record;
    } catch(e) {
      LoggerService.error('Failed to record unwilling traveller', e);

      throw e;
    }
  }
}
