import { CreateTables } from "./db2migrations";
import { CreateRTOPTables } from "./db2rtopmigrations";
import { LoggerService } from "../logs/logger";

(async () => {
    try {
        await CreateTables(true);
        await CreateRTOPTables(true);
        LoggerService.info('Successfully migrated db');
    } catch(e) {
        LoggerService.error('Failed to migrate db', e);
    }    
})();
