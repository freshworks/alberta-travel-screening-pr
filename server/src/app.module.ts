import { Module } from '@nestjs/common';
import { ArrivalFormModule } from './arrival-form/arrival-form.module';
import { AuthModule } from './auth/auth.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { ConfigModule } from '@nestjs/config';
import { HealthController } from './arrival-form/health.controller';
import { WinstonModule } from 'nest-winston';
import * as winston from 'winston';
import 'winston-daily-rotate-file';
import { NestJSConsoleFormatter } from './logs/nestjswinstonformatter';

@Module({
  imports: [
    WinstonModule.forRoot({
      transports: [
        new winston.transports.Console({
          format: NestJSConsoleFormatter,
          level: 'info',
        }),
        new winston.transports.DailyRotateFile({
          level: 'info',
          filename: 'alberta-health-%DATE%.log',
          dirname: '/logs',
          datePattern: 'YYYY-MM-DD',
          zippedArchive: true,
          maxFiles: '3650d'   // keep logs for 10 years
        })
      ]
    }),
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    ArrivalFormModule,
    ...(process.env.NODE_ENV === 'production' ? [ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', '..','..', 'client', 'build'),
      exclude: ['/api/*']
    })] : []),
    AuthModule,
  ],
  controllers: [HealthController]
})
export class AppModule { }
