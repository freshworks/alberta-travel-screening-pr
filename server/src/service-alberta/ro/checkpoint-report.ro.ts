interface CheckPointReportValueRO{
    [key:string] : number
}

export class CheckPointResultRO {

    reportField: string;
    values: CheckPointReportValueRO;

    constructor(reportField:string, dbValues: any[]) {
        this.reportField = reportField;

        this.values = {};
        dbValues.forEach(t => {
            this.values[t['ENTRY']] = t['COUNT']
        });
    }
}
