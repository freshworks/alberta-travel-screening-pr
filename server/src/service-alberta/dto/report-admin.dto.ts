import { IsString, Length, IsArray, Validate, IsIn, ValidateNested, IsEmail, IsNotEmpty } from "class-validator";
import { MaxArrayLength } from "../../decorators";
import { REPORTS } from "../constants";


export class AgenReportAccessDTO {

    @IsString()
    @Length(1, 512)
    @IsEmail()
    @IsNotEmpty()
    agentEmail: string;

    @IsArray()
    @Validate(MaxArrayLength, [3], { message: 'Cannot submit more than 3 report access' })
    @IsIn(Object.values(REPORTS),{each:true})
    reportName: string[];
}