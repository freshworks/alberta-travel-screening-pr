import { IsDate } from "class-validator";
import { Type } from "class-transformer";


export class ReportDTO{
    @IsDate()
    @Type(() => Date)
    toDate:string;
    
    @IsDate()
    @Type(() => Date)
    fromDate:string;
}