/**
 * Simple reverse proxy to be run in front of app to replicate functionality
 * provided by Kubernetes Ingress Auth.
 * 
 * 1. Authenticate all requests using the ibmcloud-appid WebAppStrategy.
 * This redirects the user to AppID if not authenticated. Once logged in,
 * a cookie is set in the users browser and the user is redirected back to the app.
 * 2. Calls to the proxy are forwarded to the app. If authenticated (step 1),
 * an Authorization header is added to the request containing the access token/id token retrieved
 */
const express = require('express');
const session = require('express-session')
const cookieSession = require('cookie-session')
const passport = require('passport');
const log4js = require('log4js');
const cors = require('cors');
const proxy = require('express-http-proxy');
const morgan = require('morgan');
const WebAppStrategy = require("ibmcloud-appid").WebAppStrategy;
const app = express();
const nocache = require('nocache');

const CALLBACK_URL = "/auth_callback";
const port = 3001;
const logger = log4js.getLogger("BorderPilot - Dev");


const setupApp = () => {
    app.use(passport.initialize());
    app.use(nocache());
    app.use(cors());
    app.use(session({
        secret: '123456',
        cookie: {
            name: 'appid-auth'
        }
    }));

    app.use(morgan('tiny'));
};

const configurePassport = () => {
    // Set up AppID Web App Strategy
    passport.use(new WebAppStrategy({
        tenantId: process.env.TENANT_ID,
        clientId: process.env.APP_ID_CLIENT_ID,
        secret: process.env.APP_ID_CLIENT_SECRET,
        oauthServerUrl: process.env.OAUTH_SERVER_URL,
        redirectUri: `http://localhost:3001${CALLBACK_URL}`
    }));
    
    passport.serializeUser(function(user, cb) {
        cb(null, user);
    });
    
    passport.deserializeUser(function(obj, cb) {
        cb(null, obj);
    });
}

const setupEndpoints = () => {
    // Logout URL
    app.get('/appid_logout', function(req, res) {
        WebAppStrategy.logout(req);
        res.redirect('/monitoring');
    });

    // URL for callback from AppID on successful login
    app.get(CALLBACK_URL, passport.authenticate(WebAppStrategy.STRATEGY_NAME));

    // Allow all calls to /static/ without auth
    app.all('/static/*', proxy('localhost:3000'));

    // Authenticate all requests with Passport AppID WebAppStrategy
    app.all('*', passport.authenticate(WebAppStrategy.STRATEGY_NAME), function(req, res, next){
        next();
    });


    // Proxy all requests to app running locally
    // Add Authorization header to request based on request session set by Passport WebAppStrategy
    app.all('*', proxy('localhost:3000', {
        proxyReqOptDecorator: (proxyReqOpts, req) => {
            if(req.session) {
                const {accessToken, identityToken} = req.session.APPID_AUTH_CONTEXT;    
                proxyReqOpts.headers['Authorization'] = `Bearer ${accessToken} ${identityToken}`;
            }

            return proxyReqOpts;
        }
    }));
}

setupApp();
configurePassport();
setupEndpoints();

app.listen(port, function(){
	logger.info(`Listening on port ${port}`);
});
