#!make

####################################################################
## Define default environment variables for local development
####################################################################

-include .env

export $(shell sed 's/=.*//' .env)

export GIT_LOCAL_BRANCH?=$(shell git rev-parse --abbrev-ref HEAD)
export DEPLOY_DATE?=$(shell date '+%Y%m%d%H%M')
export COMMIT_SHA?=$(shell git rev-parse --short=7 HEAD)
export IMAGE_TAG=${COMMIT_SHA}

export PROJECT := $(or $(PROJECT),abhealth)
export DB_USER := $(or $(DB_USER),db2inst1)
export DB_PASSWORD := $(or $(DB_PASSWORD),development)
export DB_NAME := $(or $(DB_NAME),testdb)
export DB_SERVER := $(or $(DB_SERVER),database)
export DB_PORT := $(or $(DB_PORT),50000)
export GIT_LOCAL_BRANCH := $(or $(GIT_LOCAL_BRANCH),dev)
export RECORDS_TO_SEED := 25

define deployTag
"${PROJECT}-${DEPLOY_DATE}"
endef

####################################################################
## Status Output
####################################################################

print-status:
	@echo " +---------------------------------------------------------+ "
	@echo " | Current Settings                                        | "
	@echo " +---------------------------------------------------------+ "
	@echo " | GIT LOCAL BRANCH: $(GIT_LOCAL_BRANCH) "
	@echo " | PROJECT: $(PROJECT) "
	@echo " | DB_NAME: $(DB_NAME) "
	@echo " | DB_SERVER: $(DB_SERVER) "
	@echo " | DB_USER: $(DB_USER) "
	@echo " +---------------------------------------------------------+ "

####################################################################
## Local Development
####################################################################

run-local:
	@echo "+\n++ Make: Running locally ...\n+"
	@docker-compose -f docker-compose.dev.yml up

run-local-client:
	@echo "+\n++ Make: Running locally ...\n+"
	@docker-compose -f docker-compose.dev.yml up client

run-local-server:
	@echo "+\n++ Make: Running locally ...\n+"
	@docker-compose -f docker-compose.dev.yml up server

run-local-db:
	@echo "+\n++ Make: Running db locally ...\n+"
	@docker-compose -f docker-compose.dev.yml up database

close-local:
	@echo "+\n++ Make: Closing local container ...\n+"
	@docker-compose -f docker-compose.dev.yml down

local-client-workspace:
	@docker exec -it $(PROJECT)-client sh

local-server-workspace:
	@docker exec -it $(PROJECT)-server bash

local-db-workspace:
	@docker exec -it $(PROJECT)-database bash

local-db-seed:
	@docker exec -it $(PROJECT)-server npm run db:seed --records=$(or $(records),$(RECORDS_TO_SEED))

pipeline-db-seed:
	@docker exec --env NODE_ENV=$(NODE_ENV) --env DAILY_QUESTIONS_FILE=$(DAILY_QUESTIONS_FILE) -it $(PROJECT)-server npm run db:seed --records=$(or $(records),$(RECORDS_TO_SEED)) --cleardb

local-db-migrate:
	@docker exec -it $(PROJECT)-server npm run db:migrate

cypress-integration:
	@echo "+\n++ Make: Spinning up testing GUI locally ...\n+"
	@cd ./client && npm run cypress:open

cypress-headless:
	@echo "+\n++ Make: Spinning up testing GUI locally ...\n+"
	@cd ./client && npm run cypress:run-headless

cypress-pipeline:
	@echo "+\n++ Make: Spinning up testing GUI locally ...\n+"
	@cd ./client && npm run cypress:pipeline

run-test:
	@echo "+\n++ Make: Running test containers ...\n+"
	@docker-compose -f docker-compose.test.yml up -d

test-db-migrate:
	@docker exec -i $(PROJECT)-server npm run test:migrate

test-cypress-pipeline:
	@docker exec -i $(PROJECT)-client npm run cypress:pipeline

test-server-pipeline:
	@docker exec -i $(PROJECT)-server npm run test:pipeline

print-logs:
	@docker logs $(PROJECT)-server

close-test:
	@echo "+\n++ Make: Closing test container ...\n+"
	@docker-compose -f docker-compose.test.yml down

print-envs:
	@echo "$(CYPRESS_IBM_USER) $(CYPRESS_IBM_PASS) $(CYPRESS_IBM_TOKEN)"
